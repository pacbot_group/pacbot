# PACBOT

Project PacBot ANR-20-CE10-0005

## Getting started

```sh
git clone https://gricad-gitlab.univ-grenoble-alpes.fr/pacbot_group/pacbot.git
cd pacbot
git submodule sync
git submodule update --init --recursive
git submodule update --recursive --remote
git pull --recurse-submodules=true
```
> Note: You might use: `rosdep install --from-paths src --ignore-src -r -y` to install any mising dependency to avoid the failure in catkin build.

![Architecture](Architecture.drawio.png "My ROS Architecture")
